//
//  Service.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

final class Service {
    private let baseURL: String = "https://api.github.com/users/"

    private init() {}

    static let shared = Service()

    func fetchFollowers(for username: String, page: Int = 1, completion: @escaping (Result<[Follower], Error>) -> Void) {
        let urlString = baseURL + "\(username)/followers?per_page=100&page=\(page)"
        if let url = URL(string: urlString) {
            fetchJSONData(url: url, completion: completion)
        } else {
            completion(.failure(GFError.invalidData))
        }
    }

    func fetchUserInfo(for username: String, completion: @escaping (Result<User, Error>) -> Void) {
        let urlString = baseURL + "\(username)"
        if let url = URL(string: urlString) {
            fetchJSONData(url: url, completion: completion)
        } else {
            completion(.failure(GFError.invalidData))
        }
    }
}

extension Service {
    private func fetchJSONData<T: Decodable>(url: URL, completion: @escaping (Result<T, Error>) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            } else {
                //guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { fatalError("JSON Error: HTTP response status code is not 200") }
                guard let data = data else { fatalError("JSON Error: Data fetched is nil") }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let objects = try decoder.decode(T.self, from: data)
                    completion(.success(objects))
                } catch let jsonErr {
                    print("JSON Error:", jsonErr.localizedDescription)
                    completion(.failure(jsonErr))
                }
            }
        }.resume()
    }
}

