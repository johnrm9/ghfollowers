//
//  GFBodyLabel.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class GFBodyLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    convenience init(textAlignment: NSTextAlignment = .center, numberOfLines: Int = 1) {
        self.init()
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
        configure()
    }

    private func configure() {
        textColor = .secondaryLabel
        font = .preferredFont(forTextStyle: .body)
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.75
        lineBreakMode = .byWordWrapping
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
