//
//  FollowerCell.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/8/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

class FollowerCell: UICollectionViewCell {
    static var reuseIdentifier: String { .init(describing: self) }

    private let avatarImageView: UIImageView = GFAvatarImageView()
    private let usernameLabel: UILabel = GFTitleLabel(fontSize: 16)

    init() {
        super.init(frame: .zero)
        configure()
        translatesAutoresizingMaskIntoConstraints = false
    }

    private func configure() {
        let padding: CGFloat = 8

        addSubviews(avatarImageView, usernameLabel)
//        avatarImageView.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(padding)
//            make.leading.trailing.equalToSuperview().inset(padding)
//            make.height.equalTo(avatarImageView.snp.width)
//        }
//
//        usernameLabel.snp.makeConstraints { make in
//            make.top.equalTo(avatarImageView.snp.bottom).offset(12)
//            make.leading.trailing.equalToSuperview().inset(padding)
//            make.height.equalTo(20)
//        }

        NSLayoutConstraint.activate([
            avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding),
            avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            avatarImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            avatarImageView.heightAnchor.constraint(equalTo: avatarImageView.widthAnchor),

            usernameLabel.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: 12),
            usernameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            usernameLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
