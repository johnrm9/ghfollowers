//
//  GFEmptyStateView.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/11/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

class GFEmptyStateView: UIView {

    init(message: String) {
        super.init(frame: .zero)
        messageLabel.text = message
        configure()
    }

    private lazy var messageLabel: UILabel = {
        let label = GFTitleLabel(textAlignment: .center, fontSize: 28) as UILabel
        label.numberOfLines = 3
        label.textColor = .secondaryLabel
        self.addSubview(label)
        // TODO: Remove line below when SnapKit constraints are added.
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        let empty_state_logoImage = #imageLiteral(resourceName: "empty-state-logo")
        imageView.image = empty_state_logoImage
        self.addSubview(imageView)
        // TODO: Remove line below when SnapKit constraints are added.
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    private func configure() {
        let padding: CGFloat = 40

//        messageLabel.snp.makeConstraints { make in
//            make.centerY.equalToSuperview().offset(-150)
//            make.leading.trailing.equalToSuperview().inset(padding)
//            make.height.equalTo(200)
//        }
//
//        logoImageView.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(1.3)
//            make.height.equalTo(self.snp.width).multipliedBy(1.3)
//            make.trailing.equalToSuperview().offset(170)
//            make.bottom.equalToSuperview().offset(padding)
//        }

        NSLayoutConstraint.activate([
            messageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -150),
            messageLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding),
            messageLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -padding),
            messageLabel.heightAnchor.constraint(equalToConstant: 200),

            logoImageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.3),
            logoImageView.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.3),
            logoImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 170),
            logoImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: padding)
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
