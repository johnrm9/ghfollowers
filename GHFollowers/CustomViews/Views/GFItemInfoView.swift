//
//  GFItemInfoView.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/12/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

enum ItemInfoType {
    case repos, gists, followers, following
}

class GFItemInfoView: UIView {

    private lazy var symbolImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.tintColor = .label
        self.addSubview(imageView)
        // TODO: Remove line below when SnapKit constraints are added.
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = GFTitleLabel(textAlignment: .left, fontSize: 14) as UILabel
        self.addSubview(label)
        // TODO: Remove line below when SnapKit constraints are added.
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var countLabel: UILabel = {
        let label = GFTitleLabel(textAlignment: .center, fontSize: 14) as UILabel
        self.addSubview(label)
        // TODO: Remove line below when SnapKit constraints are added.
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    private func configure() {

//        symbolImageView.snp.makeConstraints { make in
//            make.top.equalToSuperview()
//            make.leading.equalToSuperview()
//            make.width.height.equalTo(20)
//        }
//
//        titleLabel.snp.makeConstraints {make in
//            make.centerY.equalTo(symbolImageView.snp.centerY)
//            make.leading.equalTo(symbolImageView.snp.trailing).offset(12)
//            make.trailing.equalToSuperview()
//            make.height.equalTo(18)
//        }
//
//        countLabel.snp.makeConstraints {make in
//            make.top.equalTo(symbolImageView.snp.bottom).offset(4)
//            make.leading.trailing.equalToSuperview()
//            make.height.equalTo(18)
//        }

        NSLayoutConstraint.activate([
            symbolImageView.topAnchor.constraint(equalTo: self.topAnchor),
            symbolImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            symbolImageView.widthAnchor.constraint(equalToConstant: 20),
            symbolImageView.heightAnchor.constraint(equalToConstant: 20),

            titleLabel.centerYAnchor.constraint(equalTo: symbolImageView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: symbolImageView.trailingAnchor, constant: 12),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 18),

            countLabel.topAnchor.constraint(equalTo: symbolImageView.bottomAnchor, constant: 4),
            countLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            countLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            countLabel.heightAnchor.constraint(equalToConstant: 18)
        ])
    }

    func set(itemInfoType: ItemInfoType, withCount count: Int) {

        switch itemInfoType {
        case .repos:
            symbolImageView.image = UIImage(systemName: SFSymbols.repos)
            titleLabel.text = "Public Repos"
        case .gists:
            symbolImageView.image = UIImage(systemName: SFSymbols.gists)
            titleLabel.text = "Public Gists"
        case .followers:
            symbolImageView.image = UIImage(systemName: SFSymbols.followers)
            titleLabel.text = "Followers"
        case .following:
            symbolImageView.image = UIImage(systemName: SFSymbols.following)
            titleLabel.text = "Following"
        }

        countLabel.text = String(count)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
