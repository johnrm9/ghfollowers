//
//  GFAvatarImageView.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/8/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class GFAvatarImageView: UIImageView {

    private let placeholderImage: UIImage = {
        guard let image = UIImage(named: "avatar-placeholder") else { fatalError("Missing avatar-placeholder image asset") }
        return image
    }()

    private func configure() {
        layer.cornerRadius  = 10
        clipsToBounds = true
        image = placeholderImage
        translatesAutoresizingMaskIntoConstraints = false
    }

    init() {
        super.init(frame: .zero)
        configure()
    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        configure()
//    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
