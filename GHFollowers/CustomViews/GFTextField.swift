//
//  GFTextField.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class GFTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        configure()
    }

    convenience init(placeholder: String, backgroundColor: UIColor = .tertiarySystemBackground) {
        self.init(frame: .zero)
        self.placeholder = placeholder
        self.backgroundColor = backgroundColor
    }

    private func configure() {
        layer.cornerRadius = 10
        layer.borderWidth = 2
        layer.borderColor = .systemGray4

        textColor = .label
        tintColor = .label

        textAlignment = .center
        font = .preferredFont(forTextStyle: .title2)
        adjustsFontSizeToFitWidth = true
        minimumFontSize = 12

        backgroundColor = .tertiarySystemBackground
        autocorrectionType = .no
        autocapitalizationType = .none
        placeholder = "Enter a username"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
