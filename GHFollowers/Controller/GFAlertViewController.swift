//
//  GFAlertViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

class GFAlertViewController: UIViewController {

    private let alertTitle, message, buttonTitle: String

    init(title: String, message: String, buttonTitle: String) {
        self.alertTitle = title
        self.message = message
        self.buttonTitle = buttonTitle
        super.init(nibName: nil, bundle: nil)

        configure()
    }

    private lazy var containerView: UIView =  {
        let view = UIView()
        view.backgroundColor = .systemBackground
        view.layer.cornerRadius = 16
        view.layer.borderWidth = 2
        view.layer.borderColor = .white
        self.view.addSubview(view)
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = GFTitleLabel(fontSize: 20) as UILabel
        label.text = self.alertTitle
        self.containerView.addSubview(label)
        return label
    }()

    private lazy var  messageLabel: UILabel = {
        let label = GFBodyLabel(numberOfLines: 4) as UILabel
        label.text = self.message
        self.containerView.addSubview(label)
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = GFButton(backgroundColor: .systemPink) as UIButton
        button.setTitle(self.buttonTitle, for: .normal)
        button.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        self.containerView.addSubview(button)
        return button
    }()

    private func configure() {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle   = .crossDissolve
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .alertBackground
        setupViews()
    }

    private func setupViews() {
        let padding: CGFloat = 20

        containerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(280)
            make.height.equalTo(220)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(padding)
            make.leading.trailing.equalToSuperview().inset(padding)
            make.height.equalTo(28)
        }

        actionButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-padding)
            make.leading.trailing.equalToSuperview().inset(padding)
            make.height.equalTo(44)
        }

        messageLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(padding)
            make.bottom.equalTo(actionButton.snp.top).offset(-12)
        }
    }

     required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
