//
//  FollowerListViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class FollowerListViewController: UIViewController {

    private let username: String

    init(username: String) {
        self.username = username
        super.init(nibName: nil, bundle: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true

        title = username

        getFollowers(for: username)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FollowerListViewController {
    private func getFollowers(for username: String) {
         Service.shared.fetchFollowers(for: username) { result in
            switch result {
            case .success(let followers):
                print(followers)
            case .failure(let error):
                self.presentGFAlert(title: "Failed to fetch followers", message: error.localizedDescription)
            }
        }
    }
}
