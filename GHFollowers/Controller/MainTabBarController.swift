//
//  MainTabBarController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/5/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        UITabBar.appearance().tintColor = .systemGreen

        viewControllers = [
            UINavigationController(rootViewController: SearchViewController(), title: "Search", tabBarItem: .search),
            UINavigationController(rootViewController: FavoritesListViewController(), title: "Favorites", tabBarItem: .favorites)
        ]
    }
}
