//
//  UserInfoViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/9/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

class UserInfoViewController: UIViewController {

    private let username: String

    init(username: String) {
        self.username = username
        super.init(nibName: nil, bundle: nil)
    }

    private lazy var headerView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        // TODO: Remove test background color.
        view.backgroundColor = .magenta
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissViewController))
        setupViews()

        //getUserInfo(for: username)
    }

    private func setupViews() {

        headerView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(180)
        }
    }

    private func getUserInfo(for username: String) {
        Service.shared.fetchUserInfo(for: username) { [weak self] result in
            switch result {
            case .success(let user):
                print(user)
            case .failure(let error):
                self?.presentGFAlert(title: "Failed to get user info", message: error.localizedDescription)
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
