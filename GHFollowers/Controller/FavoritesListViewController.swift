//
//  FavoritesListViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/5/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

class FavoritesListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBlue
    }
}
