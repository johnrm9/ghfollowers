//
//  SearchViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/5/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SnapKit
import UIKit

class SearchViewController: UIViewController {

    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        let githubLogo = #imageLiteral(resourceName: "gh-logo")
        imageView.image = githubLogo
        self.view.addSubview(imageView)
        return imageView
    }()

    private lazy var usernameTextField: UITextField = {
        let textField = GFTextField() as UITextField
        textField.delegate = self
        self.view.addSubview(textField)
        return textField
    }()

    private lazy var getFollowersButton: UIButton = {
        let button = GFButton(backgroundColor: .systemGreen, title: "Get Followers") as UIButton
        button.addTarget(self, action: #selector(getFollowersButtonTapped(_:)), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }()

    @objc private func getFollowersButtonTapped(_ sender: UIButton) {
        pushFollowerListViewController()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground

        view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing)))
        setupViews()
    }

    private func pushFollowerListViewController() {
        if let username = usernameTextField.text, !username.isEmpty {
            navigationController?.pushViewController(FollowerListViewController(username: username), animated: true)
        } else {
            presentGFAlert(title: "Empty Username", message: "Please enter a username. We need to know who to look for 😀.")
        }
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pushFollowerListViewController()
        return true
    }
}

extension SearchViewController {
    private func setupViews() {
        let padding: CGFloat = 50

        logoImageView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(80)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(200)
        }

        usernameTextField.snp.makeConstraints { make in
            make.top.equalTo(logoImageView.snp.bottom).offset(48)
            make.leading.trailing.equalToSuperview().inset(padding)
            make.height.equalTo(padding)
        }

        getFollowersButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-padding)
            make.leading.trailing.equalToSuperview().inset(padding)
            make.height.equalTo(padding)
        }
    }
}
