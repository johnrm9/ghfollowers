//
//  Follower.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

struct Follower: Codable {
    let login: String
    let avatarUrl: String
}
