//
//  Constants.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/11/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

enum SFSymbols {
    static let location: String  = "mappin.and.ellipse"
    static let repos: String     = "folder"
    static let gists: String     = "text.alignleft"
    static let followers: String = "heart"
    static let following: String = "person.2"
}
