//
//  GFError.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

//enum GFErrorType: String {
//    case invalidUsername    = "This username created an invalid request. Please try again."
//    case unableToComplete   = "Unable to complete your request. Please check your internet connection"
//    case invalidResponse    = "Invalid response from the server. Please try again."
//    case invalidData        = "The data received from the server was invalid. Please try again."
//}

enum GFError: Error {
    case invalidUsername
    case unableToComplete
    case invalidResponse
    case invalidData
}

extension GFError: LocalizedError {
    var errorDescription: String? {
        switch self {

        case .invalidUsername:
            return NSLocalizedString(
                "This username created an invalid request. Please try again.",
                comment: ""
            )
        case .unableToComplete:
            return NSLocalizedString(
                "Unable to complete your request. Please check your internet connection",
                comment: ""
            )
        case .invalidResponse:
             return NSLocalizedString(
                "Invalid response from the server. Please try again.",
                comment: ""
            )
        case .invalidData:
             return NSLocalizedString(
                "The data received from the server was invalid. Please try again.",
                comment: ""
            )
        }
    }
}
