//
//  Extension+CGColor.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/10/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension CGColor {
    static var white: CGColor {
        UIColor.white.cgColor
    }

    static var systemGray4: CGColor {
        UIColor.systemGray4.cgColor
    }
}
