//
//  Extension+UINavigationController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/10/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension UINavigationController {
    convenience init(rootViewController: UIViewController, title: String, tabBarItem: UITabBarItem) {
        self.init(rootViewController: rootViewController)
        rootViewController.title =  title
        rootViewController.tabBarItem = tabBarItem
    }
}
