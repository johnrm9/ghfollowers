//
//  Extension+Date.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/11/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

extension Date {
    func convertToMonthYearFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        return dateFormatter.string(from: self)
    }
}

