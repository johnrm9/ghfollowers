//
//  Extension+UIViewController.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension UIViewController {

    func presentGFAlert(title: String, message: String, buttonTitle: String = "OK") {
        DispatchQueue.main.async {
            self.present(GFAlertViewController(title: title, message: message, buttonTitle: buttonTitle), animated: true)
        }
    }
}

extension UIViewController {
    @objc func dismissViewController() {
        dismiss(animated: true)
    }
}

