//
//  Extension+UITabBarItem.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/10/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension UITabBarItem {
    static var search: UITabBarItem {
        .init(tabBarSystemItem: .search, tag: 0)
    }
    static var favorites: UITabBarItem {
        .init(tabBarSystemItem: .favorites, tag: 1)
    }
}
