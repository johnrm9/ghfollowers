//
//  Extension+UIColor.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension UIColor {
    static var alertBackground: UIColor = .init(red: 0, green: 0, blue: 0, alpha: 0.75)
}
