//
//  Extension+String.swift
//  GHFollowers
//
//  Created by John R. Martin on 5/11/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

extension String {
    static var emailFormat: String {
        "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    }
    //Regex restricts to 8 character minimum, 1 capital letter, 1 lowercase letter, 1 number
    //If you have different requirements a google search for "password requirement regex" will help
    static var passwordFormat: String {
        "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
    }

    static var phoneNumberFormat: String {
        "^\\d{3}-\\d{3}-\\d{4}$"
    }
}

extension String {

    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", String.emailFormat).evaluate(with: self)
    }

    var isValidPassword: Bool {
        NSPredicate(format: "SELF MATCHES %@", String.passwordFormat).evaluate(with: self)
    }

    var isValidPhoneNumber: Bool {
        NSPredicate(format: "SELF MATCHES %@", String.phoneNumberFormat).evaluate(with: self)
    }

    func removeWhitespaces() -> String {
        components(separatedBy: .whitespaces).joined()
    }
}

extension String {

    func convertToDate() -> Date? {
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale        = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone      = .current

        return dateFormatter.date(from: self)
    }

    func convertToDisplayFormat() -> String {
        guard let date = self.convertToDate() else { return "N/A" }
        return date.convertToMonthYearFormat()
    }
}
